export const DOMAIN_NAME = 'https://sami-dish.herokuapp.com'

export const FETCH_SOFTWARE_LIST = DOMAIN_NAME+'/mobile_api/softwares'

export const FETCH_NEWS_LIST = DOMAIN_NAME+'/mobile_api/news'